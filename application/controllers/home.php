<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
    
	public function __construct(){
		parent::__construct();
    }

	public function index()
	{
		 $bread = array();
		 $d = array(
			 'menu'    => '0',
             'linkweb' => $bread,   
	         'seo'     => $this->seo(),    
	         'header'  => 'header_view',
	         'content' => 'home_view',
	         'footer'  => 'footer_view',
	         'function'=>  array('fun_home.php'),
	         'cssPlug' =>  array('jquery.bxslider.css','jquery.fancybox.css?v=2.1.5'),//plugin css
	         'script'  =>  array('jquery.bxslider.min.js','jquery.fancybox.js?v=2.1.5')// plugin js
		);
		$this->load->view('frontend/template', $d);
	}
	
	private function seo()
	{
		$robots         = "index,follow";
		$description    = "Thinkercorp.com";
		$keywords       = "";
		$title          = "Thinkercorp.com";
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name=\"robots\" content=\"".$robots."\" />";
		$meta		   .= "<META name=\"description\" content=\"".$description."\" />";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}
}