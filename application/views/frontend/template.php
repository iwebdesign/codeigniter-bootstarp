<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<?=$seo; ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="<?=base_url('css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" href="<?=base_url('css/app.css'); ?>">
		
  <!--[if IE]>
      <link href="<?=base_url('css/ie.css');?>" media="screen, projection" rel="stylesheet" type="text/css" />
  <![endif]-->
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
			ga('create', 'UA-xxxxxxxx-x', 'auto');
			ga('send', 'pageview');
		</script>
	</head>
	<body>
		
		
        <?=$this->load->view('frontend/'.$header);?>
        <?=$this->load->view('frontend/'.$content);?>	
        <?=$this->load->view('frontend/'.$footer);?>	
		
		
		
		
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=base_url();?>js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="<?=base_url('js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('js/plugins.js');?>"></script>
<script type="text/javascript" src="<?=base_url('js/vendor/jquery.cookie.js');?>"></script>
<script src="<?=base_url('plugin/sweetalert2.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('js/respond.js');?>"></script>

		<?php if(!empty($script)){
			$length = count($script);
   			 for ($i = 0; $i < $length; $i++) {
			$path   =  FCPATH."plugin/".$script[$i];
			?>
        <script type="text/javascript" src="<?=base_url();?>plugin/<?=$script[$i];?>"></script>
		<?php }  } ?>


		<?php if(!empty($function)){
		$length2 = count($function);
		for ($x = 0; $x < $length2; $x++) {
  		  echo $this->load->view('function/'.$function[$x]);
		 }  } ?>		

	</body>
</html>
